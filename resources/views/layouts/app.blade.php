<html>
    <head>
    </head>
    <body>
    <div class="nav">
    @yield('navigation')
    </div>
    <h1>@yield('title')</h1>


    <div class="foreach">
    @yield('foreach','')
    </div>
    </body>
</html>